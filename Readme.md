# Apprends à lire (en français)

Ces activités permettent aux élèves de s'exercer à lire à travers divers
exercices analogues à ceux que l'on peut trouver dans un livre de lecture de
première ou deuxième année.

La banque d'image utilisée a été adaptée (dans la mesure de nos possibilités)
au contexte culturel de l'Affrique de l'Ouest.

Vous y trouverez des activités telles que:
 * Associer un mot à son image.
 * Trouver la syllabe manquante d'un mot.
 * Remettre dans l'ordre les syllabes ou les lettres formant un mot.
 * Écrire le mot (dictée).

Les séries de mots sont classées en 7 niveaux de difficulté qui suivent la
progression habituelle des livres d'apprentissage de la lecture : on commence
par des syllabes simples (en P T M L R), puis on va progressivement vers des
syllabes plus complexes.

Ces activités ont été créées bénévolement dans le cadre du [Projet Afrikalan](https://www.afrikalan.org),
qui vise à rendre accessibles les logiciels libres éducatifs en Afrique de
l'Ouest. Elles sont diffusées gratuitement et de manière non-commerciale,
sous les termes de la licence GNU-GPL.

## Compiler le projet
Ces activités ont été créées avec android studio, vous pouvez les compiler en y
important le projet.

## Modifier les activités. 

Les activités ont été produites grace à JClic, si besoin est nous pouvons
fournir les fichiers originaux, ainsi que différents outils permetant
de (re)générer automatiquement les activités au format JClic à partir
d'un répertoire contenant les texte et les images.

N'hésitez pas à [nous contacter](mailto:contact@afrikalan.org) si besoin.


---





# Learn reading (in french)

These activities allow children to practice reading through various                                                                                          
exercises similar to those done at school during first or second grade.                                                                                      
                                                                                                                                                             
The image bank used has been adapted (to the best of our ability)                                                                                            
to the cultural context of West Africa.                                                                                                                      
                                                                                                                                                             
You will find activities such as:                                                                                                                            
 * Associate a word with its image.                                                                                                                           
 * Find the missing syllable of a word.                                                                                                                       
 * Put in order the syllables or letters forming a word.                                                                                                      
 * Write the word (dictation).                                                                                                                                
                                                                                                                                                             
The word sets are classified into 7 difficulty levels which follow the                                                                                       
usual progression of the teaching of reading: starting by simple syllables                                                                                   
(in P T M L R), then we go gradually towards more complex syllables.

These activities were created voluntarily as part of the [Afrikalan Project](https://www.afrikalan.org)),
which aims to make free educational software accessible in West Africa. 
They are distributed free of charge and in a non-commercial manner,
under the terms of the GNU-GPL license.

## Compiling the project
These activities were created with android studio, you can compile them by
importing the project in android studio.

## Modify activities.

The activities were produced thanks to JClic, if necessary we can
provide the original files, as well as various tools allowing
automatically (re)generate activities in JClic format from
a directory containing text and images.

Fell free to [contact us](mailto:contact@afrikalan.org) if you need.
