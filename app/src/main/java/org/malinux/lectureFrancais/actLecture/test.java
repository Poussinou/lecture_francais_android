package org.malinux.lectureFrancais.actLecture;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
//import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.IOException;


public class test extends AppCompatActivity {
    private static final String TAG = test.class.getSimpleName();
    private WebView myWebView;
    String refIndex;

    boolean doubleBackToExitPressedOnce = false;
    Toast toastBackBtn;

    Handler timeOutToastBack=new Handler();

    Runnable rCancelBack = new Runnable() { public void run() {doubleBackToExitPressedOnce=false;} };

    public void goToMenu()
    {
        myWebView.post(new Runnable() {
            @Override
            public void run() {
                myWebView.loadUrl("file:///android_asset/".concat(refIndex));
            }
        });
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }

    public void openUrl(String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            doubleBackToExitPressedOnce=false;
            toastBackBtn.cancel();
            timeOutToastBack.removeCallbacks(rCancelBack);

            if (myWebView.canGoBack()) myWebView.goBack();
            else super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        if (myWebView.canGoBack()) toastBackBtn=Toast.makeText(this, this.getString(R.string.toastBack_menu), 2000);
        else toastBackBtn=Toast.makeText(this, this.getString(R.string.toastBack_sortir), 2000);
        toastBackBtn.show();

        timeOutToastBack.postDelayed(rCancelBack, 2000);
    }

    public void evtChAct(String activity){
        if (activity.startsWith("presentation_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (activity.startsWith("assoc_66_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (activity.startsWith("assoc_66b_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (activity.startsWith("syl_manquante_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (activity.startsWith("ordonner_syl_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        if (activity.startsWith("assoc_18_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (activity.startsWith("ordonner_lettres_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (activity.startsWith("ordonner_lettres_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (activity.startsWith("dict_menu"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        else if (activity.startsWith("dict_"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        myWebView = (WebView) findViewById(R.id.monweb);
//        myWebView.setWebChromeClient(new WebChromeClient());
        if (android.os.Build.VERSION.SDK_INT >=19) myWebView.setWebContentsDebuggingEnabled(true);
        myWebView.getSettings().setAppCacheEnabled(false);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        myWebView.getSettings().setDefaultTextEncodingName("utf-8");
        if (android.os.Build.VERSION.SDK_INT >=17) myWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        AudioManager objAudioMan = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        objAudioMan.setStreamVolume(AudioManager.STREAM_MUSIC, objAudioMan.getStreamMaxVolume (AudioManager.STREAM_MUSIC ), 0);


        //Adaptation des réglages selon la version d'android installée
        refIndex="menu/index_jclicHtml5.html";
        int tailleMinLong=850;
        int tailleMinLarg=475;

        if (android.os.Build.VERSION.SDK_INT < 22) {
            // ancien lecteur pour anciennes versions
            refIndex="menu/index_jqclic.html";
            tailleMinLong=1024;
            tailleMinLarg=600;
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        int screenHeight = displaymetrics.heightPixels;

        //

        //Adaptation du zoom
        int tailleEcranLong=Math.max(screenWidth, screenHeight);
        int tailleEcranLarg=Math.min(screenWidth, screenHeight);
        int ratioZoomLong=(tailleEcranLong*100)/tailleMinLong;
        int ratioZoomLarg=(tailleEcranLarg*100)/tailleMinLarg;
        int ratioZoom=Math.min(ratioZoomLong, ratioZoomLarg);
        //Log.d("zoom", String.valueOf(ratioZoom));
        myWebView.setInitialScale (ratioZoom);

        myWebView.getSettings().setJavaScriptEnabled(true);
        AudioInterface audioInt=new AudioInterface(this);
        myWebView.addJavascriptInterface(audioInt, "externalAudioPlayer");
        EvtHandler evtHand=new EvtHandler(this, this);
        myWebView.addJavascriptInterface(evtHand, "externalEvtHandler");


//        if (android.os.Build.VERSION.SDK_INT >= 24) {


            myWebView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    // do your handling codes here, which url is the requested url
                    // probably you need to open that url rather than redirect:
            //        Log.d("URL", url);
                    view.loadUrl(url);
            //        view.loadDataWithBaseURL(url, null, "text/html", "utf-8", null);
                    return false; // then it is not handled by default action
                }
            });
//        }
       this.goToMenu();


    }

}
