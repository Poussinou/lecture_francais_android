package org.malinux.lectureFrancais.actLecture;

/**
 * Created by Moussa Camara on 30/01/2017.
 */

import java.io.IOException;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
//import android.util.Log;
import android.webkit.JavascriptInterface;


public class AudioInterface {
    Context mContext;
    private static final String TAG = test.class.getSimpleName();

    AudioInterface(Context c) {
        mContext = c;
    }

    //Play an audio file from the webpage
    @JavascriptInterface
    public void playAudio(String cheminRessource) { //String aud - file name passed
        //from the JavaScript function
        final MediaPlayer mp;

        //Log.d(TAG, "play idiovisuel");
        //Log.d(TAG, cheminRessource);


        try {
            AssetFileDescriptor fileDescriptor =
                    mContext.getAssets().openFd(cheminRessource);
            mp = new MediaPlayer();
            mp.setVolume(1,1);
            mp.setDataSource(fileDescriptor.getFileDescriptor(),
                    fileDescriptor.getStartOffset(),
                    fileDescriptor.getLength());
            fileDescriptor.close();
            mp.prepare();
            mp.start();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();

                };
            });
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}