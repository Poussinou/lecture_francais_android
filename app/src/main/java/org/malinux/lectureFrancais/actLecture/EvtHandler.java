package org.malinux.lectureFrancais.actLecture;

import android.content.Context;
import android.webkit.JavascriptInterface;

public class EvtHandler {
    Context mContext;
    test act;
    EvtHandler(Context c, test act) {
        this.mContext = c;
        this.act=act;
    }

    @JavascriptInterface
    public void evtLoadAct(String project, String sequence, String activity)
    {
        this.act.evtChAct(activity);
    }

    @JavascriptInterface
    public void evtJumpSeq(String path)
    {
        if (path.startsWith("menu.jclic"))
            this.act.goToMenu();
    }

    @JavascriptInterface
    public void openUrl(String url)
    {
        this.act.openUrl(url);
    }

}
