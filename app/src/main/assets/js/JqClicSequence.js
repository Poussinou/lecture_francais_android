//
// Gère la séquence
//

function GestionSequence()
{
	var objSequence=this;
	
	objSequence.initialiser=function()
	{
	}
	
	objSequence.chargerActivite=function(cle)
	{
		objSequence.cleActCourante=cle;
		objSequence.paramsActCourante=JqClic_Sequence[objSequence.cleActCourante];
		objSequence.idActCourante=objSequence.paramsActCourante.id_act;
		objSequence.ObjActCourante=new Activite(objSequence.idActCourante);
		if (objSequence.ObjActCourante.prete)
      objSequence.ObjActCourante.afficher();
    else 
      objSequence.allerActiviteSuivante();
	}
	
	objSequence.allerActivitePrecedante=function()
	{
		objSequence.chargerActivite((objSequence.cleActCourante+JqClic_Sequence.length-1)%JqClic_Sequence.length);
	}
	
	objSequence.allerActiviteSuivante=function()
	{
		objSequence.chargerActivite((objSequence.cleActCourante+1)%JqClic_Sequence.length);
	}
	
	objSequence.RechargeActiviteCourante=function()
	{	
		objSequence.chargerActivite(objSequence.cleActCourante);
	}
	
	//réalise l'action consistant à avancer dans la séquence (il ne s'agit pas toujours d'aller à l'activité suivante)
	objSequence.avancer=function()
	{
		objSequence.allerActiviteSuivante(); //temporaire
	}
	
	//réalise l'action consistant à reculer dans la séquence (il ne s'agit pas toujours d'aller à l'activité précédante)
	objSequence.reculer=function()
	{
		objSequence.allerActivitePrecedante(); //temporaire
	}
	
	objSequence.evtClickPrecedant=function()
	{
		objSequence.reculer();
	}
	
	objSequence.evtClickSuivant=function()
	{
		objSequence.avancer();
	}
	
	objSequence.evtClickRecharger=function()
	{
		objSequence.RechargeActiviteCourante();
	}
	
	objSequence.evtActiviteReussie=function()
	{
		if (objSequence.paramsActCourante.avancement_auto)
			setTimeout(objSequence.avancer, objSequence.paramsActCourante.avancement_auto_delai*1000);
	}
	
	objSequence.initialiser();
}
