function GestionBarreOutil()
{
	var objBarre=this;
	
	objBarre.initialiser=function()
	{
		objBarre.objJqBarre=$("<DIV ID=barre></DIV>").appendTo("body");
		
		//Bloc de boutons
		objBarre.ojbJqBlocBoutons=$("<DIV CLASS=\"blocBarre blocBoutons\"></DIV>").appendTo(objBarre.objJqBarre);
		objBarre.ojbJqBoutonPrec=$("<BUTTON></BUTTON>").button({icons: {primary: "icone-precedant"}}).appendTo(objBarre.ojbJqBlocBoutons).click(function(){ JqcGlob_Sequence.evtClickPrecedant(); });
		objBarre.ojbJqBoutonSuiv=$("<BUTTON></BUTTON>").button({icons: {primary: "icone-suivant"}}).appendTo(objBarre.ojbJqBlocBoutons).click(function(){ JqcGlob_Sequence.evtClickSuivant(); });
		objBarre.ojbJqBoutonRech=$("<BUTTON></BUTTON>").button({icons: {primary: "icone-recharger"}}).appendTo(objBarre.ojbJqBlocBoutons).click(function(){ JqcGlob_Sequence.evtClickRecharger(); });
	}
	
	objBarre.initialiser();
}