// function chargerSons()
// {
// 	var match_extention;
// 	var extensions_son=['wav', 'ogg', 'mp3'];
// 	var bloc_medias=$("<DIV ID=blocMedias></DIV>").appendTo("body");
//   
// 	for (var nomMedia in JqClic_Medias)
// 	{
// 		if (match_extention=/[.]([^.]*)$/g.exec(JqClic_Medias[nomMedia]))
// 			if ((extensions_son.indexOf(match_extention[1]))!=-1)
// 				//console.log("<audio id=\"son_"+nomMedia+"\" src=\""+JqClic_Medias[nomMedia]+"\" controls preload=\"auto\" autobuffer></audio>");
// 				$("<audio id=\"son_"+nomMedia+"\" src=\""+JqClic_Medias[nomMedia]+"\" controls preload=\"auto\" autobuffer></audio>").appendTo(bloc_medias);
// 
// 
// 	}
// 	  
// }


function LecteurSon()
{
	var lecteur=this;
	lecteur.WpAddHook=WpAddHook;
	lecteur.WpCallHooks=WpCallHooks;
	
	lecteur.initialiser=function()
	{
		lecteur.objJqBlocLecteur=$("<DIV ID=blocMedias></DIV>").appendTo("body");
		lecteur.objJqLecteur=$("<AUDIO ID=Lecteur CONTROLS AUTOBUFFER></AUDIO>").bind("ended", lecteur.evtLectureFinie).appendTo(lecteur.objJqBlocLecteur);
		lecteur.occupe=false;
	}
	
	lecteur.evtLectureFinie=function()
	{
		lecteur.occupe=false;
		lecteur.WpCallHooks("lectureFinie");
	}
	
	lecteur.lireSonUrl=function(url)
	{
		if (typeof externalAudioPlayer!="undefined")
		{
		    var baseCheminAsset= window.location.href.replace(/\/[^\/]*$/, "").replace(/^file:\/\/\/android_asset\/?/, "" );
		    baseCheminAsset=decodeURIComponent(baseCheminAsset);
 			externalAudioPlayer.playAudio(baseCheminAsset!=''?baseCheminAsset+"/"+url:url);
			lecteur.WpCallHooks("lectureFinie"); //on a transmi a endroid, donc c'est libre, on dit que "c'est fini"
			lecteur.occupe=false;
			return(true); //Son déjà traité, la suite ne s'execute pas...
		}
		
		if(lecteur.occupe) alert("erreur: lecture depuis un lecteur occupe!!!");
		lecteur.occupe=true;
		lecteur.objJqLecteur[0].src=url;
		lecteur.objJqLecteur[0].play();
	}
	
	lecteur.initialiser();
}


function GestionnaireSon()
{
	var gestionnaire=this;
	gestionnaire.nbLecteurs=4;
	
	gestionnaire.initialiser=function()
	{
		gestionnaire.arSonsProjet=JqClic_SonsSys;	//contient soit le son de base, soit un son défini par l'utilisateur
		gestionnaire.arSonsActivite={};
		gestionnaire.listeLecture=[];
		
		gestionnaire.arObjLecteurs=[];
		for (var i=0; i<gestionnaire.nbLecteurs; i++)
		{
			gestionnaire.arObjLecteurs[i]=new LecteurSon();
			gestionnaire.arObjLecteurs[i].WpAddHook("lectureFinie", gestionnaire.evtLectureFinie);
		}
	}
	
	gestionnaire.evtLectureFinie=function()
	{
		if (gestionnaire.listeLecture.length>0)
			gestionnaire.lireSon(gestionnaire.listeLecture.shift());
	}
	
	gestionnaire.integrerSonsActivite=function(arSons)
	{
		gestionnaire.arSonsActivite=arSons;
	}
  
	gestionnaire.lireSonEvt=function(idEvt)
	{
		if(typeof gestionnaire.arSonsActivite[idEvt]!="undefined") gestionnaire.lireSon(gestionnaire.arSonsActivite[idEvt]);
		else if (typeof gestionnaire.arSonsProjet[idEvt]!="undefined") gestionnaire.lireSon(gestionnaire.arSonsProjet[idEvt]);
		else alert("le son de l'évènement ["+idEvt+"] est introuvable!!!");
	}
	
	gestionnaire.lireSon=function(idSon)
	{
	  
		if (typeof JqClic_Medias[idSon]=="undefined") { alert(idSon+" n'existe pas dans la librairie média"); return(false); }

		if (JqClic_Medias[idSon].type!="son") { alert(idSon+" n'est pas un son"); return(false); }

		
		var boolSonTraite=false;
		for (var i in gestionnaire.arObjLecteurs)
			if(!gestionnaire.arObjLecteurs[i].occupe)
			{
				gestionnaire.arObjLecteurs[i].lireSonUrl(JqClic_Medias[idSon].chemin);
				boolSonTraite=true;
				break;
			}

		if(!boolSonTraite) gestionnaire.listeLecture.push(idSon);
	}
	
	gestionnaire.initialiser();
}